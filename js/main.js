var settingsVisible = false;
var settings = null;
const defaultSettings = {timeFormat: "default", wallpaper: "default"};

loadSettings();

const timeLbl = document.getElementById("time");
const backgroundDiv = document.getElementById("background");
const settingsElem = document.getElementById("settings");
const settingsDialog = document.getElementById("settings-dialog");
const settingsButton = document.getElementById("settings-button");
const settingsCloseButton = document.getElementById("settings-close-button");
const timeFormatSelect = document.getElementById("time-format-select");
const wallpaperSelect = document.getElementById("wallpaper-select");

updateTime();
updateWallpaper();

setInterval(updateTime, 1000);

function updateTime() {
  var timeText = "";
  switch(settings.timeFormat) {
    case "24h":
      timeText = get24HourTimeString();
      break;
    default:
        timeText = new Date().toLocaleTimeString();
  }
  timeLbl.innerText = timeText;
}

function updateWallpaper() {
  switch(settings.wallpaper) {
    case "unsplash":
      backgroundDiv.style.backgroundImage = `url(\"https://source.unsplash.com/random/1920x1080/\")`;
      break;
    default:
      backgroundDiv.style.background = "";
  }
}

function get24HourTimeString() {
  var date = new Date();
  var hours = date.getHours().toString();
  var minutes = date.getMinutes().toString();
  var seconds = date.getSeconds().toString();
  
  if(hours.length === 1) {
    hours = "0" + hours;
  }

  if(minutes.length === 1) {
    minutes = "0" + minutes;
  }

  if(seconds.length === 1) {
    seconds = "0" + seconds;
  }

  return `${hours}:${minutes}:${seconds}`;
}

function showSettingsDialog() {
  settingsPrepare();
  settingsElem.style.display = "";
  settingsVisible = true;
}

function settingsPrepare() {
  timeFormatSelect.value = settings.timeFormat;
  timeFormatSelect.onchange = e => {
    settings.timeFormat = e.target.value;
    updateTime();
    saveSettings();
  };
  wallpaperSelect.value = settings.wallpaper;
  wallpaperSelect.onchange = e => {
    settings.wallpaper = e.target.value;
    updateWallpaper();
    saveSettings();
  }
}

function closeSettingsDialog() {
  settingsElem.classList.add("fadeout");
  settingsDialog.classList.add("fadeout");
  setTimeout(function() {
    settingsElem.style.display = "none";
    settingsElem.classList.remove("fadeout");
    settingsDialog.classList.remove("fadeout");
    settingsVisible = false;
  }, 500);
}

settingsButton.onclick = () => showSettingsDialog();
settingsElem.onclick = function(e) {
  if(e.target === settingsElem) {
    closeSettingsDialog();
  }
}
settingsCloseButton.onclick = () => closeSettingsDialog();

function saveSettings() {
  var json = JSON.stringify(settings);
  window.localStorage.newtabSettings = json;
}

function loadSettings() {
  var json = window.localStorage.newtabSettings;
  if(!json) {
    settings = Object.assign({}, defaultSettings);
  } else {
    settings = JSON.parse(json);
  }
}